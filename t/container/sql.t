use strict;
use warnings;
use Test::More;
use Test::Moose;
use Class::Load 0.20 'load_class';

my $t = new_ok load_class('Feeder::Container::Mapper::Test::SQL');

foreach my $type ( $t->_types ) {
	my $writer = $t->resolve( service => '/writer/'. lc $type );
	my $reader = $t->resolve( service => '/reader/'. lc $type );
	isa_ok  $writer, 'Feeder::Mapper::SQL::Writer::' . $type;
	isa_ok  $reader, 'Feeder::Mapper::SQL::Reader::' . $type;
	does_ok $writer, 'Feeder::Interface::Mapper::Writer';
	does_ok $reader, 'Feeder::Interface::Mapper::Reader';
}

done_testing;
