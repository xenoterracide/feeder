use strict;
use warnings;
use Test::More;
use Class::Load 0.20 'load_class';

my $repo   = new_ok load_class('Feeder::Container::Repository');
my $mapper = new_ok load_class('Feeder::Container::Mapper::Test');

$repo->add_sub_container( $mapper );

isa_ok $repo->resolve( service => 'feed' ), 'Feeder::Repository::Feed';

done_testing;
