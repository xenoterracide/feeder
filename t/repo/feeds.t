use strict;
use warnings;
use Test::Moose;
use Test::More;
use Class::Load 0.20 'load_class';

my $mc = load_class('Feeder::Container::Mapper::Test')->new;

my $feeds
	= new_ok( load_class('Feeder::Repository::Feed') => [{
		_reader => $mc->resolve( service => '/mapper/reader/feed' ),
		_writer => $mc->resolve( service => '/mapper/writer/feed' ),
	}]);

does_ok $feeds, 'Feeder::Interface::Repository';

my $feed0 = $feeds->get( id => 'o7iFk9LYh_ovSaslcFqve3bKVjU');
my $feed1 = $feeds->create( uri => 'http://example.com' );

foreach my $feed ( $feed0, $feed1 ) {
	isa_ok $feed, 'Feeder::Model::Feed';
}

done_testing;
