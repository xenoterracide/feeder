use strict;
use warnings;
use Test::More;
use Test::Method;
use Test::Deep;
use Class::Load 'load_class';
use DDP;

my $model_c = load_class('Feeder::Model::Feed');

my $feed0
	= new_ok( $model_c => [{
		uri => 'http://feeds.feedburner.com/xenoterracide',
	}]);

method_ok $feed0, 'id' => [], 'o7iFk9LYh_ovSaslcFqve3bKVjU';

my $feed1
	= new_ok( $model_c => [{
		uri => 'http://ironman.enlightenedperl.org/?feed=atom',
	}]);

method_ok $feed1, 'id' => [], 'BDtYtG-XaTg4GG1iu_0MR2mVUso';

foreach my $entry ( $feed0->entries ) {
	can_ok $entry, 'title';
	can_ok $entry, 'content';
	can_ok $entry, 'id';
	note $entry->id;
}

done_testing;
