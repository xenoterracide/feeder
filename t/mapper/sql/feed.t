use strict;
use warnings;
use Test::More;
use Test::Moose;
use Test::Fatal;
use Class::Load 0.20 'load_class';
use DDP;

load_class('DBIx::QueryLog')->enable           if $ENV{SQL_DEBUG};
load_class('Log::Any::Adapter')->set('Stderr') if $ENV{TEST_DEBUG};
my $model_c = load_class('Feeder::Model::Feed');

my $c = load_class('Feeder::Container::Mapper::Test::SQL')->new;

my $writer = $c->resolve( service => '/writer/feed' );
my $reader = $c->resolve( service => '/reader/feed' );

does_ok $writer, 'Feeder::Interface::Mapper::Writer';
can_ok  $writer, 'save';

does_ok $reader, 'Feeder::Interface::Mapper::Reader';
can_ok  $reader, 'load';

my $feed0 = $model_c->new({ uri => 'http://example.com' });

my $e0
	= exception {
		$writer->save( $feed0 );
	};

my $e1
	= exception {
		$writer->save( $feed0 );
	};

ok ! defined $e0, 'first attempt to save succeeds';
isa_ok $e1, 'Throwable::Error';

my $feed1 = $model_c->new( $reader->load( id => $feed0->id ) );

isa_ok $feed1, $model_c;

done_testing;

## cleanup
my ( $sql, @bind )
	= $writer->_sqla->delete( feeds => { feed_id => $feed0->id } )
	;

$writer->_db->dbh->do( $sql, undef, @bind );
