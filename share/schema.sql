DROP TABLE IF EXISTS feeds;
CREATE TABLE feeds (
	feed_id
		TEXT
		NOT NULL
		UNIQUE
		,
	uri
		TEXT
		NOT NULL
		UNIQUE
		,
	PRIMARY KEY ( uri ),
	UNIQUE ( feed_id, uri )
);
