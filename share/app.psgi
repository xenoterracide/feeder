#!/usr/bin/env perl
use strict;
use warnings;
use Plack::Builder;
use Class::Load 0.20 'load_class';

my $feeder = load_class('Feeder::App')->new;

builder {
	enable 'LogAny';
	enable 'Static',
		path => qr{^/(index.*|img|css|js)}xms,
		root => $feeder->resolve( service => '/static')->stringify
		;
	$feeder->to_app;
};
