package Feeder::Container::Mapper::Test;
use 5.016;
use strict;
use warnings;
use namespace::autoclean;

our $VERSION = '0.001000'; # VERSION

use Moose;
use Bread::Board::Declare;
use Bread::Board;
use Class::Load 0.20 'load_class';

with qw(
	Feeder::Container::Role::Mapper
);

has '+name' => ( default => 'mapper' );

sub _build_namespace {
	return 'Feeder::Mapper::Test';
}

sub _deps { ## no critic ( ProhibitUnusedPrivateSubroutines )
	return +{}
}

__PACKAGE__->meta->make_immutable;
1;
# ABSTRACT: Locator for our test mapper

__END__

=pod

=head1 NAME

Feeder::Container::Mapper::Test - Locator for our test mapper

=head1 VERSION

version 0.001000

=head1 AUTHOR

Caleb Cushing <xenoterracide@gmail.com>

=head1 COPYRIGHT AND LICENSE

This software is Copyright (c) 2013 by Caleb Cushing.

This is free software, licensed under:

  The GNU Affero General Public License, Version 3, November 2007

=cut
