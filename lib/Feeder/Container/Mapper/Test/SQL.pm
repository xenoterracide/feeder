package Feeder::Container::Mapper::Test::SQL;
use 5.016;
use strict;
use warnings;
use namespace::autoclean;
use Test::Requires::Env qw( FEEDER_DB_DSN );

our $VERSION = '0.001000'; # VERSION

use Moose;
use Bread::Board::Declare;
use Class::Load 0.20 'load_class';

extends 'Feeder::Container::Mapper::SQL';

has '+dsn'      => ( block => sub { $ENV{FEEDER_DB_DSN}  }, required => 0 );
has '+username' => ( block => sub { $ENV{FEEDER_DB_USER} // '' } );
has '+password' => ( block => sub { $ENV{FEEDER_DB_PASS} // '' } );

__PACKAGE__->meta->make_immutable;
1;
# ABSTRACT: simple testing class

__END__

=pod

=head1 NAME

Feeder::Container::Mapper::Test::SQL - simple testing class

=head1 VERSION

version 0.001000

=head1 AUTHOR

Caleb Cushing <xenoterracide@gmail.com>

=head1 COPYRIGHT AND LICENSE

This software is Copyright (c) 2013 by Caleb Cushing.

This is free software, licensed under:

  The GNU Affero General Public License, Version 3, November 2007

=cut
