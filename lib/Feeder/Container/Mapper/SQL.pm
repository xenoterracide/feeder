package Feeder::Container::Mapper::SQL;
use 5.016;
use strict;
use warnings;
use namespace::autoclean;

our $VERSION = '0.001000'; # VERSION

use Moose;
use Bread::Board::Declare;
use Bread::Board;
use Class::Load 0.20 'load_class';

with qw(
	Feeder::Container::Role::Mapper
);

sub _build_namespace {
	return 'Feeder::Mapper::SQL';
}

sub _deps { ## no critic ( ProhibitUnusedPrivateSubroutines )
	return {
		_db   => '../_db',
		_log  => '/_logger',
		_sqla => '../_sqla',
	};
}

has dsn => (
	isa      => 'Str',
	is       => 'ro',
	required => 1,
);

has username => (
	isa      => 'Str',
	is       => 'ro',
);

has password => (
	isa      => 'Str',
	is       => 'ro',
);

has _sqla => (
	isa       => 'SQL::Abstract::More',
	is        => 'ro',
	lifecycle => 'Singleton',
);

has _db => (
	isa          => 'DBIx::Connector',
	is           => 'ro',
	lifecycle    => 'Singleton',
	dependencies => [qw( dsn username password )],
	block     => sub {
		my $s = shift;

		load_class('DBIx::Connector')->new(
			$s->param('dsn'), $s->param('username'), $s->param('password'),
		);
	},
);

__PACKAGE__->meta->make_immutable;
1;
# ABSTRACT: SQL Mapper Injector

__END__

=pod

=head1 NAME

Feeder::Container::Mapper::SQL - SQL Mapper Injector

=head1 VERSION

version 0.001000

=head1 AUTHOR

Caleb Cushing <xenoterracide@gmail.com>

=head1 COPYRIGHT AND LICENSE

This software is Copyright (c) 2013 by Caleb Cushing.

This is free software, licensed under:

  The GNU Affero General Public License, Version 3, November 2007

=cut
