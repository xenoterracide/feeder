package Feeder::Container::Repository;
use 5.016;
use strict;
use warnings;
use namespace::autoclean;

our $VERSION = '0.001000'; # VERSION

use Moose;
use Bread::Board;
use Bread::Board::Declare;

with qw(
	Feeder::Container::Role::Common
);

has '+name' => ( default => 'repo' );

sub BUILD {
	my $self = shift;

	container $self => as {
		foreach my $type ( $self->_types ) {
			service lc $type => (
				class        => join( '::', $self->_namespace, $type ),
				lifecycle    => 'Singleton',
				dependencies => $self->_deps( $type ),
			);
		}
	};
	return;
}

sub _build_namespace {
	return 'Feeder::Repository';
}

sub _deps { ## no critic ( ProhibitUnusedPrivateSubroutines )
	my ( $self, $type ) = @_;
	return {
		_log            => '/_logger',
		_reader         => lc "mapper/reader/$type",
		_writer         => lc "mapper/writer/$type",
	};
}

__PACKAGE__->meta->make_immutable;
1;

#ABSTRACT: Repository Service Locator

__END__

=pod

=head1 NAME

Feeder::Container::Repository - Repository Service Locator

=head1 VERSION

version 0.001000

=head1 AUTHOR

Caleb Cushing <xenoterracide@gmail.com>

=head1 COPYRIGHT AND LICENSE

This software is Copyright (c) 2013 by Caleb Cushing.

This is free software, licensed under:

  The GNU Affero General Public License, Version 3, November 2007

=cut
