package Feeder::Container::Role::Common;
use strict;
use warnings;
use namespace::autoclean;

our $VERSION = '0.001000'; # VERSION

use Moose::Role;
use Bread::Board::Declare;
use MooseX::Types::Perl qw( PackageName );

with qw(
	Feeder::Container::Role::Logger
);

has _types => (
	isa     => 'ArrayRef[Str]',
	traits  => [qw( Array ) ],
	is      => 'bare',
	lazy    => 1,
	service => 0,
	handles => {
		_types => 'elements',
	},
	default => sub { [qw( Feed )] },
);

has _namespace => (
	isa     => PackageName,
	is      => 'ro',
	lazy    => 1,
	builder => '_build_namespace',
	service => 0,
);

1;
# ABSTRACT: Common to all locators

__END__

=pod

=head1 NAME

Feeder::Container::Role::Common - Common to all locators

=head1 VERSION

version 0.001000

=head1 AUTHOR

Caleb Cushing <xenoterracide@gmail.com>

=head1 COPYRIGHT AND LICENSE

This software is Copyright (c) 2013 by Caleb Cushing.

This is free software, licensed under:

  The GNU Affero General Public License, Version 3, November 2007

=cut
