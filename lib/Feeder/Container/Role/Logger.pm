package Feeder::Container::Role::Logger;
use strict;
use warnings;
use namespace::autoclean;

our $VERSION = '0.001000'; # VERSION

use Moose::Role;
use Bread::Board::Declare;
use MooseX::Types::LogAny qw( LogAny );
use Class::Load 0.20 'load_class';

has _logger => (
	isa       => LogAny,
	is        => 'ro',
	lifecycle => 'Singleton',
	block     => sub { load_class('Log::Any')->get_logger },
);

1;

# ABSTRACT: class has a logger

__END__

=pod

=head1 NAME

Feeder::Container::Role::Logger - class has a logger

=head1 VERSION

version 0.001000

=head1 AUTHOR

Caleb Cushing <xenoterracide@gmail.com>

=head1 COPYRIGHT AND LICENSE

This software is Copyright (c) 2013 by Caleb Cushing.

This is free software, licensed under:

  The GNU Affero General Public License, Version 3, November 2007

=cut
