package Feeder::Container::Role::Mapper;
use 5.016;
use strict;
use warnings;
use namespace::autoclean;

our $VERSION = '0.001000'; # VERSION

use Moose::Role;
use Bread::Board::Declare;
use Bread::Board;

with qw(
	Feeder::Container::Role::Common
);

sub BUILD {
	my $self = shift;

	container $self => as {
		foreach my $mapper (qw( Reader Writer ) ) {
			container lc $mapper => as {
				foreach my $type ( $self->_types ) {
					service lc $type => (
						class     => join( '::',
								$self->_namespace, $mapper, $type
							),
						lifecycle    => 'Singleton',
						dependencies => $self->_deps,
					);
				}
			};
		}
	};
	return;
}

1;

# ABSTRACT: Mappers

__END__

=pod

=head1 NAME

Feeder::Container::Role::Mapper - Mappers

=head1 VERSION

version 0.001000

=head1 AUTHOR

Caleb Cushing <xenoterracide@gmail.com>

=head1 COPYRIGHT AND LICENSE

This software is Copyright (c) 2013 by Caleb Cushing.

This is free software, licensed under:

  The GNU Affero General Public License, Version 3, November 2007

=cut
