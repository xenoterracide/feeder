package Feeder::Interface::Mapper::Writer;
use 5.010;
use strict;
use warnings;
use namespace::autoclean;

our $VERSION = '0.001000'; # VERSION

use Moose::Role;
use Type::Params    qw( compile );
use Types::Standard qw( Object  );

around save => sub {
	my $orig = shift;
	my $self = shift;

	state $check = compile( Object );
	my ( $obj ) = $check->( @_ );

	return $self->$orig( $obj );
};

1;
# ABSTRACT: Interface for Mapper Writers

__END__

=pod

=head1 NAME

Feeder::Interface::Mapper::Writer - Interface for Mapper Writers

=head1 VERSION

version 0.001000

=head1 AUTHOR

Caleb Cushing <xenoterracide@gmail.com>

=head1 COPYRIGHT AND LICENSE

This software is Copyright (c) 2013 by Caleb Cushing.

This is free software, licensed under:

  The GNU Affero General Public License, Version 3, November 2007

=cut
