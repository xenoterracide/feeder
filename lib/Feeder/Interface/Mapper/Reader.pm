package Feeder::Interface::Mapper::Reader;
use 5.010;
use strict;
use warnings;
use namespace::autoclean;

our $VERSION = '0.001000'; # VERSION

use Moose::Role;
use Type::Params    qw( compile );
use Types::Standard qw( Str     );

around load => sub {
	my $orig = shift;
	my $self = shift;

	state $check = compile( Str, Str );
	my ( $key, $value ) = $check->( @_ );

	return $self->$orig( $key, $value );
};

1;
# ABSTRACT: Mapper Reader Interface

__END__

=pod

=head1 NAME

Feeder::Interface::Mapper::Reader - Mapper Reader Interface

=head1 VERSION

version 0.001000

=head1 AUTHOR

Caleb Cushing <xenoterracide@gmail.com>

=head1 COPYRIGHT AND LICENSE

This software is Copyright (c) 2013 by Caleb Cushing.

This is free software, licensed under:

  The GNU Affero General Public License, Version 3, November 2007

=cut
