package Feeder::Interface::Repository;
use 5.010;
use strict;
use warnings;

our $VERSION = '0.001000'; # VERSION

use Moose::Role;
use Type::Params    qw( compile            );
use Types::Standard qw( slurpy HashRef Str );

around create => sub {
	my $orig = shift;
	my $self = shift;

	state $check = compile( slurpy HashRef );
	my ( $obj_args ) = $check->( @_ );

	return $self->$orig( $obj_args );
};

around get => sub {
	my $orig = shift;
	my $self = shift;

	state $check = compile( Str, Str );
	my ( $key, $val ) = $check->( @_ );

	return $self->$orig( $key, $val );
};

1;
# ABSTRACT: Repository Interface

__END__

=pod

=head1 NAME

Feeder::Interface::Repository - Repository Interface

=head1 VERSION

version 0.001000

=head1 METHODS

=head2 create

	my $obj = $repo->create( ... );

Takes constructor args as a parameter and returns the object on success.

=head2 get

	my $obj = $repo->get( key => $val );

Takes a unique key to search by and the specific value to search for, returns
the object if found.

=head1 AUTHOR

Caleb Cushing <xenoterracide@gmail.com>

=head1 COPYRIGHT AND LICENSE

This software is Copyright (c) 2013 by Caleb Cushing.

This is free software, licensed under:

  The GNU Affero General Public License, Version 3, November 2007

=cut
