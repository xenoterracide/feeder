package Feeder::Repository;
use 5.016;
use strict;
use warnings;
use namespace::autoclean;

our $VERSION = '0.001000'; # VERSION

use Moose;
use Try::Tiny;
use Class::Load 0.20 'load_class';

use MooseX::Types::Perl qw( PackageName );

with qw(
	Feeder::Interface::Repository
	Feeder::Role::Logs
);

# exception class namespace
my $xc = 'Feeder::X::';

sub new_model_instance {
	my ( $self, @args ) = @_;

	my $obj
		= try {
			$self->_model->new( @args );
		}
		catch {
			my $error = $_;
			$self->_log->debug( $error );
			load_class( $xc . 'IllegalArgument')->throw( message => $error );
		};

	return $obj;
}

sub create {
	my ( $self, @args ) = @_;

	my $obj = $self->new_model_instance( @args );

	$self->add( $obj );

	return $obj;
}

sub add {
	my ( $self, $obj ) = @_;

	try {
		$self->_writer->save( $obj );
	}
	catch {
		my $error = $_;
		$self->_log->error( $error );
		load_class( $xc . 'IllegalState')->throw( message => $error );
	};

	return;
}

sub get {
	my ( $self, $key, $value ) = @_;

	my $args = $self->_reader->load( $key, $value );

	return $self->_model->new( $args );
}

has _model => (
	isa      => PackageName,
	is       => 'ro',
	lazy     => 1,
	builder  => '_build_model',
);

has _reader => (
	does     => 'Feeder::Interface::Mapper::Reader',
	is       => 'ro',
	required => 1,
);

has _writer => (
	does     => 'Feeder::Interface::Mapper::Writer',
	is       => 'ro',
	required => 1,
);

__PACKAGE__->meta->make_immutable;
1;

# ABSTRACT: feed repository

__END__

=pod

=head1 NAME

Feeder::Repository - feed repository

=head1 VERSION

version 0.001000

=head1 AUTHOR

Caleb Cushing <xenoterracide@gmail.com>

=head1 COPYRIGHT AND LICENSE

This software is Copyright (c) 2013 by Caleb Cushing.

This is free software, licensed under:

  The GNU Affero General Public License, Version 3, November 2007

=cut
