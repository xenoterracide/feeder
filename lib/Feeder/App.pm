package Feeder::App;
use 5.016;
use strict;
use warnings;
use namespace::autoclean;

our $VERSION = '0.001000'; # VERSION

use OX;
use Class::Load 'load_class';
use MooseX::Types::LogAny 'LogAny';
use File::ShareDir::ProjectDistDir ':all', { pathclass => 1 };

my $dist = 'Feeder';

router as {
	route '/feed' => 'feed'
};

sub BUILD {
	my $self = shift;

	# rewrite to determine mapper and its config from config
	my $mapper
		= load_class('Feeder::Container::Mapper::SQL')->new(
			$self->config->{sql}
		);
	$self->add_sub_container( $mapper );
	return;
}

### Controllers ###

has feed => (
	isa      => 'Feeder::Controller::Feed',
	is       => 'ro',
	infer    => 1,
);

## General

has sharedir => (
	isa          => 'Path::Class::Dir',
	is           => 'ro',
	lifecycle    => 'Singleton',
	block        => sub { return dist_dir($dist) },
);

has static => (
	isa          => 'Path::Class::Dir',
	is           => 'ro',
	lifecycle    => 'Singleton',
	dependencies => [qw( sharedir )],
	block        => sub {
		my $s = shift;

		return $s->param('sharedir')->subdir('public');
	},
);

has config_file => (
	isa          => 'Path::Class::File',
	is           => 'ro',
	lifecycle    => 'Singleton',
	dependencies => [qw( sharedir )],
	block        => sub {
		my $s = shift;

		return $s->param('sharedir')->subdir('conf')->file('feeder.conf');
	},
);

has config => (
	isa          => 'HashRef',
	is           => 'ro',
	lifecycle    => 'Singleton',
	dependencies => ['config_file'],
	block        => sub {
		my $s = shift;

		my $cfg
			= load_class('Config::Any')->load_files({
				flatten_to_hash => 1,
				use_ext         => 1,
				files           => [ $s->param('config_file')->stringify ],
				General         => {
					-InterPolateEnv  => 1,
					-InterPolateVars => 1,
				},
			});

		return $cfg->{ $s->param('config_file') };
	},
);

has logger => (
	isa       => LogAny,
	is        => 'ro',
	lifecycle => 'Singleton',
	block     => sub {
		load_class('Log::Any::Adapter')->set('Stderr');
		return load_class('Log::Any')->get_logger;
	},
);

has json => (
	isa          => 'JSON',
	is           => 'ro',
	lifecycle    => 'Singleton',
	block        => sub { load_class('JSON')->new->utf8(1)->allow_blessed(1) },
);

1;

# ABSTRACT: A Google Reader replacement

__END__

=pod

=head1 NAME

Feeder::App - A Google Reader replacement

=head1 VERSION

version 0.001000

=head1 AUTHOR

Caleb Cushing <xenoterracide@gmail.com>

=head1 COPYRIGHT AND LICENSE

This software is Copyright (c) 2013 by Caleb Cushing.

This is free software, licensed under:

  The GNU Affero General Public License, Version 3, November 2007

=cut
