package Feeder::Mapper::Test::Role::Common;
use strict;
use warnings;
use namespace::autoclean;

our $VERSION = '0.001000'; # VERSION

use Moose::Role;
use Class::Load 0.20 'load_class';

has _cache => (
	isa     => 'CHI::Driver',
	is      => 'ro',
	lazy    => 1,
	default => sub {
		my $self = shift;

		return load_class('CHI')
			->new(
				namespace => $self->_tablename,
				driver    => 'Memory',
				global    => 1
			);
	},
);

has _tablename => (
	isa     => 'Str',
	is      => 'ro',
	lazy    => 1,
	builder => '_build_tablename',
);

1;
# ABSTRACT: Common

__END__

=pod

=head1 NAME

Feeder::Mapper::Test::Role::Common - Common

=head1 VERSION

version 0.001000

=head1 AUTHOR

Caleb Cushing <xenoterracide@gmail.com>

=head1 COPYRIGHT AND LICENSE

This software is Copyright (c) 2013 by Caleb Cushing.

This is free software, licensed under:

  The GNU Affero General Public License, Version 3, November 2007

=cut
