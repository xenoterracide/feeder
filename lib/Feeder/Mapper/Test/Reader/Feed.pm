package Feeder::Mapper::Test::Reader::Feed;
use strict;
use warnings;
use namespace::autoclean;

our $VERSION = '0.001000'; # VERSION

use Moose;
with qw(
	Feeder::Interface::Mapper::Reader
	Feeder::Mapper::Test::Role::Common
);

sub load {
	my ( $self, $key, $value ) = @_;

	return $self->_cache->get( $value );
}

sub _build_tablename {
	return 'feed';
}

sub BUILD {
	my $self = shift;

	my @feeds = (
		{
			id  => 'o7iFk9LYh_ovSaslcFqve3bKVjU',
			uri => 'http://feeds.feedburner.com/xenoterracide',
		},
	);
	foreach my $feed ( @feeds ) {
		$self->_cache->set( $feed->{id}, $feed );
	}
	return;
}

1;
# ABSTRACT: Test Feed Reader

__END__

=pod

=head1 NAME

Feeder::Mapper::Test::Reader::Feed - Test Feed Reader

=head1 VERSION

version 0.001000

=head1 AUTHOR

Caleb Cushing <xenoterracide@gmail.com>

=head1 COPYRIGHT AND LICENSE

This software is Copyright (c) 2013 by Caleb Cushing.

This is free software, licensed under:

  The GNU Affero General Public License, Version 3, November 2007

=cut
