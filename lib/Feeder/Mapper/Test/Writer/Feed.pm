package Feeder::Mapper::Test::Writer::Feed;
use strict;
use warnings;
use namespace::autoclean;

our $VERSION = '0.001000'; # VERSION

use Moose;
with qw(
	Feeder::Interface::Mapper::Writer
	Feeder::Mapper::Test::Role::Common
);

sub save {
	my ( $self, $obj ) = @_;

	return;
}

__PACKAGE__->meta->make_immutable;
1;

# ABSTRACT: Test Writer

__END__

=pod

=head1 NAME

Feeder::Mapper::Test::Writer::Feed - Test Writer

=head1 VERSION

version 0.001000

=head1 AUTHOR

Caleb Cushing <xenoterracide@gmail.com>

=head1 COPYRIGHT AND LICENSE

This software is Copyright (c) 2013 by Caleb Cushing.

This is free software, licensed under:

  The GNU Affero General Public License, Version 3, November 2007

=cut
