package Feeder::Mapper::SQL::Role::Writer;
use strict;
use warnings;
use namespace::autoclean;

our $VERSION = '0.001000'; # VERSION

use Moose::Role;
use Class::Load 0.20 'load_class';
use Try::Tiny;

with qw(
	Feeder::Mapper::SQL::Role::Common
	Feeder::Interface::Mapper::Writer
);

1;
# ABSTRACT: SQL Mapper role common to all readers

__END__

=pod

=head1 NAME

Feeder::Mapper::SQL::Role::Writer - SQL Mapper role common to all readers

=head1 VERSION

version 0.001000

=head1 AUTHOR

Caleb Cushing <xenoterracide@gmail.com>

=head1 COPYRIGHT AND LICENSE

This software is Copyright (c) 2013 by Caleb Cushing.

This is free software, licensed under:

  The GNU Affero General Public License, Version 3, November 2007

=cut
