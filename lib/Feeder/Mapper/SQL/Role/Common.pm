package Feeder::Mapper::SQL::Role::Common;
use strict;
use warnings;
use namespace::autoclean;

our $VERSION = '0.001000'; # VERSION

use Moose::Role;
use Class::Load 0.20 'load_class';

with qw(
	Feeder::Role::Logs
);

has _db => (
	isa      => 'DBIx::Connector',
	is       => 'ro',
	required => 1,
);

has _sqla => (
	isa      => 'SQL::Abstract::More',
	is       => 'ro',
	lazy     => 1,
	default  => sub { load_class('SQL::Abstract::More')->new },
);

1;
# ABSTRACT: Role common to all SQL Mappers

__END__

=pod

=head1 NAME

Feeder::Mapper::SQL::Role::Common - Role common to all SQL Mappers

=head1 VERSION

version 0.001000

=head1 AUTHOR

Caleb Cushing <xenoterracide@gmail.com>

=head1 COPYRIGHT AND LICENSE

This software is Copyright (c) 2013 by Caleb Cushing.

This is free software, licensed under:

  The GNU Affero General Public License, Version 3, November 2007

=cut
