package Feeder::Mapper::SQL::Writer::Feed;
use 5.010;
use strict;
use warnings;
use namespace::autoclean;

our $VERSION = '0.001000'; # VERSION

use Moose;
use Try::Tiny;
use Class::Load 0.20 'load_class';

with qw(
	Feeder::Mapper::SQL::Role::Writer
);

sub save {
	my ( $self, $obj ) = @_;

	try {
		$self->_db->txn( sub {
			my $results
				= $_->selectall_arrayref( $self->_select( $obj ) );

			if ( scalar @$results == 0 ) {
				$_->do( $self->_insert( $obj ) );
			}
			else {
				load_class('Throwable::Error')->throw(
					message => $obj->uri . ' exists'
				 );
			}
		});
	}
	catch {
		given ( $_ ) {
			when ( $_->isa('Throwable::Error') ) {
				$_->throw;
			}
			default {
				$self->_log->error( $_ );
				load_class('Throwable::Error')->throw( message => "$_" );
			}
		}
	};
	return;
}

sub _select {
	my ( $self, $obj ) = @_;

	my ( $sql, @bind )
		= $self->_sqla->select(
			-columns => [qw( feed_id )],
			-from    => 'feeds',
			-where   => { feed_id => $obj->id },
		);

	$self->_log->debug( $sql ) if $self->_log->is_debug;

	return ( $sql, undef, @bind );
}

sub _insert {
	my ( $self, $obj ) = @_;

	my ( $sql, @bind )
		= $self->_sqla->insert( feeds => {
			feed_id  => $obj->id,
			uri      => $obj->uri->as_string,
		});

	$self->_log->debug( $sql ) if $self->_log->is_debug;

	return ( $sql, undef, @bind );
}

__PACKAGE__->meta->make_immutable;
1;
# ABSTRACT: Feed writer

__END__

=pod

=head1 NAME

Feeder::Mapper::SQL::Writer::Feed - Feed writer

=head1 VERSION

version 0.001000

=head1 AUTHOR

Caleb Cushing <xenoterracide@gmail.com>

=head1 COPYRIGHT AND LICENSE

This software is Copyright (c) 2013 by Caleb Cushing.

This is free software, licensed under:

  The GNU Affero General Public License, Version 3, November 2007

=cut
