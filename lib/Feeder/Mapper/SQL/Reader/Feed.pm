package Feeder::Mapper::SQL::Reader::Feed;
use 5.016;
use strict;
use warnings;
use namespace::autoclean;

our $VERSION = '0.001000'; # VERSION

use Moose;

with qw(
	Feeder::Mapper::SQL::Role::Reader
);

sub load {
	my ( $self, $key, $value ) = @_;

	# fundamentally flawed
	my $db_col = $key eq 'id' ? 'feed_id' : 'uri';

	my ( $sql, @bind )
		= $self->_sqla->select(
			-columns => [qw( feed_id|id uri )],
			-from    => 'feeds',
			-where   => { $db_col => $value },
		);

	return $self->_fetchone_sql( $sql, @bind );
}

__PACKAGE__->meta->make_immutable;
1;

# ABSTRACT: Feed SQL Mapper

__END__

=pod

=head1 NAME

Feeder::Mapper::SQL::Reader::Feed - Feed SQL Mapper

=head1 VERSION

version 0.001000

=head1 AUTHOR

Caleb Cushing <xenoterracide@gmail.com>

=head1 COPYRIGHT AND LICENSE

This software is Copyright (c) 2013 by Caleb Cushing.

This is free software, licensed under:

  The GNU Affero General Public License, Version 3, November 2007

=cut
