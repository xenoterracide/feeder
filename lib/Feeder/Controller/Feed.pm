package Feeder::Controller::Feed;
use 5.016;
use strict;
use warnings;
use namespace::autoclean;

our $VERSION = '0.001000'; # VERSION

use DDP;
use Try::Tiny;
use Moose;
use Class::Load 0.20 'load_class';
use HTTP::Throwable::Factory 'http_throw';

with qw(
	Feeder::Role::Logs
);

sub get {
	return [ 200, [], ];
}

sub post {
	my ( $self, $r ) = @_;

	my $feed;
	try {
		$feed
			= $self->_feeds->create(
				$self->_json->decode( $r->content )
			);
	}
	catch {
		my $error = $_;

		$self->_log->debug( $r->content );

		if ( $error->isa('Feeder::X::IllegalArgument') ) {
			http_throw( 'BadRequest' );
		}

		# log after checking validation as that's a common problem
		$self->_log->error( $_ );

		if ( $error->isa('Feeder::X::IllegalState') ) {
			http_throw( 'Conflict' );
		}

		http_throw('InternalServerError'); # unknown exception
	};

	my $loc = $r->uri_for({ action => 'feed', id => $feed->id });

	return [ 201, [ Location => $loc ],
			$self->_json->encode( $feed )
		];
}

has _feeds => (
	isa      => 'Feeder::Repository::Feed',
	is       => 'ro',
	required => 1,
);

has _json => (
	isa      => 'JSON',
	is       => 'ro',
	required => 1,
);

__PACKAGE__->meta->make_immutable;
1;

# ABSTRACT: Feed

__END__

=pod

=head1 NAME

Feeder::Controller::Feed - Feed

=head1 VERSION

version 0.001000

=head1 AUTHOR

Caleb Cushing <xenoterracide@gmail.com>

=head1 COPYRIGHT AND LICENSE

This software is Copyright (c) 2013 by Caleb Cushing.

This is free software, licensed under:

  The GNU Affero General Public License, Version 3, November 2007

=cut
