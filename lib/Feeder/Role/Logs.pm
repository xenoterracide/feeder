package Feeder::Role::Logs;
use 5.016;
use strict;
use warnings;
use namespace::autoclean;

our $VERSION = '0.001000'; # VERSION

use Moose::Role;

use MooseX::Types::LogAny qw( LogAny );
use Class::Load 0.20 'load_class';

has _log => (
	isa     => LogAny,
	is      => 'ro',
	lazy    => 1,
	default => sub { load_class('Log::Any')->get_logger },
);

1;

# ABSTRACT: Object logs things

__END__

=pod

=head1 NAME

Feeder::Role::Logs - Object logs things

=head1 VERSION

version 0.001000

=head1 AUTHOR

Caleb Cushing <xenoterracide@gmail.com>

=head1 COPYRIGHT AND LICENSE

This software is Copyright (c) 2013 by Caleb Cushing.

This is free software, licensed under:

  The GNU Affero General Public License, Version 3, November 2007

=cut
