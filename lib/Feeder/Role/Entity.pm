package Feeder::Role::Entity;
use strict;
use warnings;
use namespace::autoclean;

our $VERSION = '0.001000'; # VERSION

use Moose::Role;

with qw(
	Feeder::Role::Logs
);

has id => (
	isa     => 'Str',
	is      => 'ro',
	lazy    => 1,
	builder => '_build_id',
);

1;
# ABSTRACT: applies to all entities

__END__

=pod

=head1 NAME

Feeder::Role::Entity - applies to all entities

=head1 VERSION

version 0.001000

=head1 AUTHOR

Caleb Cushing <xenoterracide@gmail.com>

=head1 COPYRIGHT AND LICENSE

This software is Copyright (c) 2013 by Caleb Cushing.

This is free software, licensed under:

  The GNU Affero General Public License, Version 3, November 2007

=cut
