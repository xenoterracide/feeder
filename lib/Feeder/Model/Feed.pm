package Feeder::Model::Feed;
use 5.016;
use strict;
use warnings;
use namespace::autoclean;

our $VERSION = '0.001000'; # VERSION

use Moose;
use Class::Load 0.20 'load_class';

use MooseX::Types::URI qw( Uri );

with qw(
	Feeder::Role::Entity
);

sub TO_JSON {
	my $self = shift;

	return { uri => $self->uri->as_string };
}

sub _build_id {
	my $self = shift;

	load_class('Encode');
	load_class('MIME::Base64::URLSafe');

	my $dgst = load_class('Digest')->new('SHA-1');

	# convert to 8 bit encoding for safety digest operates on bytes not
	# characters
	my $data = Encode::encode( 'UTF-8' => $self->uri );

	$dgst->add( $data );
	my $id = MIME::Base64::URLSafe::encode( $dgst->digest );

	$self->_log->debug( "new feed with id: $id" );

	return $id;
}

has uri => (
	isa      => Uri,
	is       => 'ro',
	required => 1,
	coerce   => 1,
);

has _feed => (
	isa     => 'XML::Atom::Feed',
	is      => 'ro',
	lazy    => 1,
	default => sub {
		my $self = shift;
		return $self->_atom_client->getFeed( $self->uri );
	},
	handles => [ qw( entries ) ],
);

has _atom_client => (
	isa     => 'XML::Atom::Client',
	is      => 'ro',
	lazy    => 1,
	default => sub { load_class('XML::Atom::Client')->new },
);

__PACKAGE__->meta->make_immutable;
1;

# ABSTRACT: Feed

__END__

=pod

=head1 NAME

Feeder::Model::Feed - Feed

=head1 VERSION

version 0.001000

=head1 AUTHOR

Caleb Cushing <xenoterracide@gmail.com>

=head1 COPYRIGHT AND LICENSE

This software is Copyright (c) 2013 by Caleb Cushing.

This is free software, licensed under:

  The GNU Affero General Public License, Version 3, November 2007

=cut
