package Feeder::Repository::Feed;
use 5.016;
use strict;
use warnings;
use namespace::autoclean;

our $VERSION = '0.001000'; # VERSION

use Moose;
use Class::Load 0.20 'load_class';

extends 'Feeder::Repository';

sub _build_model {
	return load_class('Feeder::Model::Feed');
}

__PACKAGE__->meta->make_immutable;
1;

# ABSTRACT: feed repository

__END__

=pod

=head1 NAME

Feeder::Repository::Feed - feed repository

=head1 VERSION

version 0.001000

=head1 AUTHOR

Caleb Cushing <xenoterracide@gmail.com>

=head1 COPYRIGHT AND LICENSE

This software is Copyright (c) 2013 by Caleb Cushing.

This is free software, licensed under:

  The GNU Affero General Public License, Version 3, November 2007

=cut
